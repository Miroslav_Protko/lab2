import os
import json
import logging


CONFIG ={
    "clear": False,
    "show": False,
    "dry_run": False,
    "force": False,
    "policy": "time",
    "max_size": 10000,
    "regex": False,
    "restore": False,
    "silent": False,
    "storage_time": "14 days, 0:0:0",
    "basket_path": "/home/miroaslav/basket"
}


def create_config(path, config_dict=None):
    """Create config file."""

    if config_dict is None:
        config_dict = {}

    file_extension = os.path.splitext(path)[-1]
    path = os.path.abspath(os.path.expanduser(path))

    with open(path, "w") as config_file:
        if file_extension == ".json":
            json.dump(config_dict, config_file)
        elif file_extension == ".txt":
            config_file.write(config_dict)
        else:
            message = """"{file} has not been created! It has a format json and txt.""".format(file=path)
            logging.warning(message)


def load_config(path):
    """Load config file and return configuration pararameters dict."""

    path = os.path.abspath(os.path.expanduser(path))
    file_extension = os.path.splitext(path)[-1]
    config = CONFIG

    if not os.path.exists(path):
        message = """The {file} doesn't exist! The default config will be used.""".format(file=path)
        logging.warning(message)

        return config

    with open(path, "r") as config_file:
        if file_extension == ".json":
            config = json.load(config_file)
        elif file_extension == ".txt":
            config_file.read()
        else:
            message = """The {file} doesn't not match the format of json and txt! 
            The default config will be used.""".format(file=path)

    return config


def update_config(path, config_dict=None):
    """Update current config file."""

    if config_dict is None:
        config_dict = {}

    path = os.path.abspath(os.path.expanduser(path))
    file_extension = os.path.splitext(path)[-1]

    if not os.path.exists(path):
        message = """The {file} doesn't exist! The default config will be used.""".format(file=path)
        logging.warning(message)

        return

    with open(path, "w") as config_file:
        if file_extension == ".json":
            json.dump(config_dict, config_file)
        elif file_extension == ".txt":
            config_file.write(config_dict)
        else:
            message = """{file} hasn't been updated! It has a format different from json and txt.""".format(file=path)
            logging.warning(message)








