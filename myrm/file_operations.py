#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import random

BASKET_PATH = os.path.expanduser('~/basket')


def get_unique_name(old_path):
    """Return unique name of file in directory."""

    filename = os.path.basename(old_path)

    unique_name = "{name}.{id}".format(name=filename, id=random.random())

    return unique_name


def get_size(basket_path=BASKET_PATH):
    """Returns the total amount of space occupied by files in the dir."""

    filesdir_path = os.path.join(basket_path, 'files/')

    _size = 0

    for root, dirs, files in os.walk(filesdir_path):
        for filename in files:
            file_path = os.path.join(root, filename)
            if not os.path.islink(filename):
                _size += os.path.getsize(file_path)
        for dirname in dirs:
            dir_path = os.path.join(root, dirname)
            if not os.path.islink(dir_path):
                _size += os.path.getsize(dir_path)

    return _size




