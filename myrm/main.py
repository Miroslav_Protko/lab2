#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse
import logging
import os


from myrm.basket_operations import remove, remove_by_regex, restore, show_basket, clear_basket, \
    recycle_by_time_policy, recycle_by_size_policy
from myrm.config_tools import create_config, update_config, load_config
from myrm.move_errors import Codes, output


CONFIG_PATH = os.path.expanduser('~/lab2/config.json')
BASKET_PATH = os.path.expanduser('~/basket')
STORAGE_TIME = "14 days, 0:0:0"
MAX_SIZE = 32 * 1024


def parse():
    """Returns an ArgumentParser with arguments added"""

    parser = argparse.ArgumentParser()
    parser.add_argument('-d', action='store_true', dest='delete', help='Delete files')
    parser.add_argument('-r', action='store_true', dest='restore', help='Restore files')
    parser.add_argument('-c', action='store_true', dest='clear', help='Clear basket')
    parser.add_argument('-re', action='store_true', dest='regex', help='Use regex')
    parser.add_argument('-show', action='store_true', dest='show', help='Show basket')
    parser.add_argument('-policy', action='store_true', dest='policy', help='Set recycle policy')
    parser.add_argument('-silent', action="store_true", help='Do not show the actions performed')
    parser.add_argument('-dry', default=False, action='store_true', dest='dry_run', help='Simulates action to be performed on console')
    parser.add_argument('-f', action='store_true', dest='force', help='Ignore non-existent files and arguments')
    parser.add_argument('-update', action='store_true',dest='update_config', help='Update config file')
    parser.add_argument('-create', action='store_true', dest='create_config', help='Create config file')
    parser.add_argument('-config', default=CONFIG_PATH, help='Specifies the path to the config file in json format')
    parser.add_argument('-basket-path', default=BASKET_PATH, help='Set the path to the basket')
    parser.add_argument('-storage-time', default=STORAGE_TIME, help='Determines how long the files should \ '
                                                                    'be stored from the time of deletion. Format: 2017-08-17 21:39:43')
    parser.add_argument('-max-size', default=MAX_SIZE, help='Sets the maximum volume value for the basket')
    parser.add_argument('filenames', nargs='*', help='Name file')

    return parser.parse_args()


def main():
    args = vars(parse())
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

    config = load_config(args['config'])

    if not os.path.exists(CONFIG_PATH):
        create_config(args['config'], args)

    if args['create_config']:
        create_config(args['config'], args)

    if args['update_config']:
        update_config(args['config'], args)

    if args['dry_run']:
        update_config(args['config'], args)

    if args['silent']:
        update_config(args['config'], args)

    if args['force']:
        update_config(args['config'], args)

    if args['filenames']:
        for filename in args['filenames']:
            code = remove(filename, basket_path=BASKET_PATH, dry=config['dry_run'],
                   silent=config['silent'], force=config['force'])
            print code

    if args['restore']:
        for filename in args['filenames']:
            code = restore(filename, basket_path=BASKET_PATH, dry=config['dry_run'],
                   silent=config['silent'], force=config['force'])
            print code

    if args['show']:
        basket_list = show_basket()
        print basket_list

    if args['clear']:
        clear_basket()

        logging.info('Files deleted.')


    if args['regex']:
        for arg in args['NAME']:
            code = remove_by_regex(arg, os.path.basename(arg), basket_path=BASKET_PATH, dry=config['dry_run'],
                   silent=config['silent'], force=config['force'])
            print code


    if args['policy']:
        for arg in args['NAME']:
            if arg =="time":
                recycle_by_time_policy()
            else:
                recycle_by_size_policy()

        logging.info('Files deleted.')


if __name__ == "__main__":
    main()

