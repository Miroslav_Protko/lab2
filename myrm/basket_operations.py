#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import shutil
import datetime
import json
import re
import logging

from myrm.file_operations import get_size, get_unique_name
from myrm.move_errors import Codes


BASKET_PATH = os.path.expanduser('~/basket')
STORAGE_TIME = datetime.timedelta(days=14)
MAX_SIZE = 32 * 1024

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')


def remove(name, basket_path=BASKET_PATH, dry=False, silent=False, force=False):
    """Removes directories and files in Basket.
       Returns old and new pathes of successfully moved files.
       If path doesn't exist, OSError will be raised.
    """

    filesdir_path, trashinfo_path = get_basket(basket_path)
    old_path = os.path.abspath(os.path.expanduser(name))
    code = Codes.GOOD

    if not os.path.exists(old_path):
        if not force:
            raise IOError("{file} not found.".format(file=name))
            code = Codes.NO_FILE

    if dry:
        if not silent:

            print(old_path, ' would be replaced to basket')

    else:

        del_date = datetime.datetime.now()

        unique_name = get_unique_name(old_path)
        create_trashinfo(old_path, unique_name, del_date, basket_path=BASKET_PATH)

        filesdir_path = os.path.join(filesdir_path, unique_name)

        os.rename(old_path, filesdir_path)

        logging.info('File removed')

    return code


def remove_by_regex(path, regex, basket_path=BASKET_PATH, dry=False, silent=False, force=False):
    """Removes files by regex."""

    found_pathes = []
    code = Codes.GOOD

    for _file in os.listdir(path):
        if os.path.isfile(_file):
            if re.match(os.path.basename(_file), regex):
                found_pathes.append(_file)
        elif os.path.isdir(_file):
            if re.match(os.path.basename(_file), regex):
                found_pathes.append(_file)
            else:
                found_pathes.extend(remove_by_regex(_file, regex))

    if found_pathes is None:
        if not force:

            logging.info('No regex mathes.')
            code = Codes.NO_FILE

    if dry:
        if not silent:

            print(found_pathes, ' would be removed')


    for filename in found_pathes:
        filename = os.path.basename(filename)
        remove(filename, basket_path)
        logging.info('File removed')

    return code


def restore(name, basket_path=BASKET_PATH, dry=False, silent=False, force=False):
    """Restores directories and files from Basket."""

    filesdir_path, trashinfo_path = get_basket(basket_path)
    code = Codes.GOOD

    trashinfo_path = os.path.join(trashinfo_path, name)
    filesdir_path = os.path.join(filesdir_path, name)

    if not os.path.exists(filesdir_path):
        if not force:
            raise IOError("{file} not found.".format(file=name))
            code = Codes.NO_FILE

    if dry:
        if not silent:

            print(filesdir_path, ' would be recovered')

    else:

        with open(trashinfo_path, "r") as info_file:
            fileinfo = json.load(info_file)

            path = fileinfo['Old path']
            os.rename(filesdir_path, path)
            os.remove(trashinfo_path)

            logging.info('File restore')

    return code


def create_trashinfo(old_path, unique_name, del_date, basket_path=BASKET_PATH):
    """Create trashinfo file"""

    filesdir_path, trashinfo_path = get_basket(basket_path)

    infodict = {'Old path': old_path, 'Delete date': del_date.strftime("%d-%m-%Y %H:%M")}

    trashinfo_path = os.path.join(trashinfo_path, unique_name)

    with open(trashinfo_path, "w") as info_file:
        json.dump(infodict, info_file)


def get_basket(basket_path=BASKET_PATH):
    """Return pathes files and info dir."""

    return os.path.join(basket_path, 'files/'), os.path.join(basket_path, 'info/')


def create_basket(basket_path=BASKET_PATH):
    """Creates basket by the specified path."""

    filesdir_path, trashinfo_path = get_basket(basket_path)

    if not os.path.exists(filesdir_path):
        os.makedirs(filesdir_path)

    if not os.path.exists(trashinfo_path):
        os.makedirs(trashinfo_path)


def show_basket(basket_path=BASKET_PATH):
    """Return list of files in Basket."""

    filesdir_path, trashinfo_path = get_basket(basket_path)

    basket_list = os.listdir(filesdir_path)

    return basket_list


def clear_basket(basket_path=BASKET_PATH):
    """Deletes directories and files in Basket."""

    filesdir_path, trashinfo_path = get_basket(basket_path)

    for trashfile in os.listdir(filesdir_path):
        trash = os.path.join(filesdir_path, trashfile)

        if os.path.isfile(trash):
            os.remove(trash)
        else:
            shutil.rmtree(trash)

    for info in os.listdir(trashinfo_path):
        os.remove(os.path.join(trashinfo_path, info))


def recycle_by_time_policy(basket_path=BASKET_PATH):
    """Check the basket for cleaning by time.

       If some file has a difference of its deletion time and
       the current time is greater than indicated in STORAGE_TIME
       than this file will be deleted.

       Return list removable filenames."""

    filesdir_path, trashinfo_path = get_basket(basket_path)
    removable_files = []

    for filename in os.listdir(trashinfo_path):
        with open(os.path.join(trashinfo_path, filename), "r") as info:
            fileinfo = json.load(info)

        del_date = fileinfo['Delete date']
        del_date = datetime.datetime.strptime(del_date, "%d-%m-%Y %H:%M")

        delta_time = datetime.datetime.now() - del_date

        if delta_time > STORAGE_TIME:
            removable_files.append(filename)

    for filename in removable_files:
        path = os.path.join(filesdir_path, filename)

        if os.path.isfile(path):
            os.remove(path)
        else:
            shutil.rmtree(path)

    for info in removable_files:
        os.remove(os.path.join(trashinfo_path, info))

    return removable_files


def recycle_by_size_policy(basket_path=BASKET_PATH):
    """Check the basket for cleaning by size.

       If the total Basket volume is greater than the MAX_SIZE
       then we clear the Basket.

       Return True if Basket was cleaned, else return False."""

    filesdir_path, trashinfo_path = get_basket(basket_path)

    basket_size = get_size(filesdir_path)
    removable_files = [os.path.join(filesdir_path, path) for path in os.listdir(filesdir_path)]

    if basket_size > MAX_SIZE:
        clear_basket(basket_path)
        return removable_files

    return []


