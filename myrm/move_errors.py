#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import sys

class Codes:
    GOOD = 0
    NO_FILE = 1
    BAD = 2


def output(silent, code, *args):
    msg = {
        1:"No such file.",
        2:"Uncknown error.",
    }

    if not silent:
        if code == Codes.GOOD.value:
            print(''.join([str(x) for x in args]))
        else:
            print(msg[code])

    sys.exit(code)