#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from os.path import join, dirname


setup(
    name='rmpy',
    version='1.0',
    include_pakages_data=True,
    packages=find_packages(),
    long_discription=open(join(dirname(__file__), 'README.md')).read(),
    entry_points={
        'console_scripts':
            ['rmpy = myrm.main:main']
        }
)
