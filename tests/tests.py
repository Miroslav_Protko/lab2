#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import unittest

from myrm.basket_operations import *
from myrm.file_operations import *

filesdir_path = '/home/miroslav/basket/files/'
trashinfo_path = '/home/miroslav/basket/info/'

dry = False
silent = False

class Test(unittest.TestCase):

    def setUp(self):
        os.mkdir("test_dir")

        files = ["a", "b", "c"]
        for file in files:
            with open("test_dir/%s.txt" % file, "w"):
                pass

    def tearDown(self):
        clear_basket()
        if os.path.exists("test_dir"):
            shutil.rmtree("test_dir")


    def test_remove_file(self):
        file = "test_dir/a.txt"
        remove(file)
        self.assertFalse(os.path.exists(file))
        self.assertTrue(os.path.exists(os.path.join(filesdir_path, "a.txt")))


    def test_remove_file_absolute(self):
        file = os.path.abspath(os.path.expanduser("test_dir/a.txt"))
        remove(file)
        self.assertFalse(os.path.exists(file))
        self.assertTrue(os.path.exists(os.path.join(filesdir_path, "a.txt")))


    def test_remove_dir(self):
        dir = "test_dir"
        remove(dir)
        self.assertFalse(os.path.exists(dir))
        self.assertTrue(os.path.exists(os.path.join(filesdir_path, dir)))


    def test_remove_dir_absolute(self):
        dir = "test_dir"
        dir_path = os.path.abspath(os.path.expanduser(dir))
        remove(dir_path)
        self.assertFalse(os.path.exists(dir_path))
        self.assertTrue(os.path.exists(os.path.join(filesdir_path, dir)))


    def test_recover_file(self):
        remove("test_dir/a.txt")
        self.assertFalse(os.path.exists("test_dir/a.txt"))
        restore("a.txt")
        self.assertTrue(os.path.exists("test_dir/a.txt"))


    def test_remove_by_regex(self):
        remove_by_regex("test_dir/*.txt", '*.txt')
        self.assertFalse(os.path.exists("test_dir/b.txt"))
        self.assertTrue(os.path.exists(os.path.join(filesdir_path, "b.txt")))

if __name__ == '__main__':
    unittest.main()