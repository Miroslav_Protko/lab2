
## Description

Console utility for deleting files / directories with support for the "Basket" and the ability to restore deleted objects.

### List of basic operations:
* Remove file / directory.
* Remove all objects by regular expression in the specified subtree (directory).
* View the "Basket" elements. 
* Manual cleaning of the basket, automatic cleaning by different policies. 
* Recovery files / directories from "Basket".

### Installation

Run:
``` bash 
sudo ./setup.py install
```

To uninstall:
``` bash 
pip uninstall rmpy
```

### Default config file

JSON:

{
    "clear": False,
    "show": False,
    "dry_run": False,
    "force": False,
    "policy": "time",
    "max_size": 10000,
    "regex": False,
    "restore": False,
    "silent": False,
    "storage_time": "14 days, 0:0:0",
    "basket_path": "/home/miroaslav/basket"
}
